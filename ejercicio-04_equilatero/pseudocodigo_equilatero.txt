Funcion validarLado(lado)
Inicio
    Si (lado>0)
    retornar TRUE
    Sino 
    retornar FALSE
    Fin-si
Fin

Inicio
    Variable lado es numerico Real
        Variable perimetro es numerico Real
    leer "Ingresa un lado ",lado
    Si (validarLado(lado))
        perimetro=lado*3
        Escribir "el perimetro del triangulo es ",perimetro
    Sino
        Escribir"Lado no valido"
    Fin-si

Fin